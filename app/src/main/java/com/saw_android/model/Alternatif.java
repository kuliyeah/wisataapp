package com.saw_android.model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.saw_android.database.SQLHelper;

import java.util.ArrayList;
import java.util.HashMap;

public class Alternatif {
    public String lokasi;
    private final SQLHelper dbHelper;
    Context contextApp;

    public Alternatif(Context context) {
        dbHelper = new SQLHelper(context);
        contextApp = context;
    }

    public ArrayList<HashMap<String, String>> getList() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String querySelect = "SELECT * From alternatif order by id_alternatif desc";
        ArrayList<HashMap<String, String>> trxList = new ArrayList<>();
        Cursor cursor = db.rawQuery(querySelect, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> trx = new HashMap<>();
                trx.put("id_alternatif", cursor.getString(cursor.getColumnIndex("id_alternatif")));
                trx.put("nama_alternatif", cursor.getString(cursor.getColumnIndex("nama_alternatif")));
                trx.put("lokasi", cursor.getString(cursor.getColumnIndex("lokasi")));
                trxList.add(trx);
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return trxList;
    }

}
