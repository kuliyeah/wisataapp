package com.saw_android.model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.saw_android.database.SQLHelper;

import java.util.ArrayList;
import java.util.HashMap;

public class Kriteria {
    private final SQLHelper dbHelper;
    Context contextApp;

    public Kriteria(Context context) {
        dbHelper = new SQLHelper(context);
        contextApp = context;
    }

    public ArrayList<HashMap<String, String>> getList() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String querySelect = "SELECT * From kriteria order by id_kriteria desc";
        ArrayList<HashMap<String, String>> trxList = new ArrayList<>();
        Cursor cursor = db.rawQuery(querySelect, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> trx = new HashMap<>();
                trx.put("id_kriteria", cursor.getString(cursor.getColumnIndex("id_kriteria")));
                trx.put("nama_kriteria", cursor.getString(cursor.getColumnIndex("nama_kriteria")));
                trx.put("kepentingan", cursor.getString(cursor.getColumnIndex("kepentingan")));
                trx.put("cost_benefit", cursor.getString(cursor.getColumnIndex("cost_benefit")));
                trxList.add(trx);
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return trxList;
    }
}