package com.saw_android.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.saw_android.R;

import java.util.ArrayList;
import java.util.HashMap;

public class KriteriaAdapter extends ArrayAdapter<HashMap<String, String>> {

    public KriteriaAdapter(Context context, ArrayList<HashMap<String, String>> trxList) {
        super(context, 0, trxList);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HashMap<String, String> tr = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_kriteria_list_data, parent, false);
        }
        TextView title = (TextView) convertView.findViewById(R.id.title);
        TextView uang = (TextView) convertView.findViewById(R.id.uang);
        TextView bobot = (TextView) convertView.findViewById(R.id.bobot);
        assert tr != null;
        title.setText(tr.get("nama_kriteria"));
        uang.setText(tr.get("cost_benefit"));
        bobot.setText("Bobot: " + tr.get("kepentingan"));
        return convertView;
    }
}
