package com.saw_android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.saw_android.R;

import java.util.ArrayList;
import java.util.HashMap;

public class AlternatifAdapter extends ArrayAdapter<HashMap<String, String>> {

    public AlternatifAdapter(Context context, ArrayList<HashMap<String, String>> trxList) {
        super(context, 0, trxList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HashMap<String, String> tr = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_alternatif_list_data, parent, false);
        }
        TextView title = (TextView) convertView.findViewById(R.id.title);
        TextView uang = (TextView) convertView.findViewById(R.id.uang);
        assert tr != null;
        title.setText(tr.get("nama_alternatif"));
        uang.setText(tr.get("lokasi"));
        return convertView;
    }
}
