package com.saw_android.view;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saw_android.R;
import com.saw_android.database.SQLHelper;

public class AnalisaSawActivity extends AppCompatActivity {

    String[] alternatif;
    String[] kriteria;
    String[] costbenefit;
    double[] kepentingan;
    double[][] alternatifkriteria;
    String[] id_alternatif;
    String[] lokasi_alternatif;
    String[] biaya_alternatif;
    String[] fasilitas_alternatif;
    String[] transportasi_alternatif;
    String[] jarak_alternatif;
    String[] id_kriteria;

    protected Cursor cursor;
    SQLHelper dbHelper;

    public LinearLayout tampiltabel(double[][] data) {
        LinearLayout.LayoutParams lpp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lpp.setMargins(1, 1, 1, 1);

        LinearLayout llv = new LinearLayout(this);
        llv.setOrientation(LinearLayout.VERTICAL);
        for (int i = 0; i < data.length; i++) {
            LinearLayout llh = new LinearLayout(this);
            llh.setOrientation(LinearLayout.HORIZONTAL);
            llv.addView(llh);

            for (int j = 0; j < data[0].length; j++) {
                TextView textview = new TextView(this);
                int maxLength = 4;
                if (maxLength > String.valueOf(data[i][j]).length()) {
                    maxLength = String.valueOf(data[i][j]).length();
                }
                textview.setText(String.valueOf(data[i][j]).substring(0, maxLength));
                textview.setWidth(250);
                textview.setGravity(Gravity.CENTER);
                textview.setBackgroundColor(Color.parseColor("#FDFDFD"));
                textview.setLayoutParams(lpp);
                textview.setPadding(2, 2, 2, 2);
                llh.addView(textview);
            }
        }
        llv.setBackgroundColor(Color.parseColor("#A1A1A1"));
        llv.setLayoutParams(lpp);
        llv.setPadding(1, 1, 1, 1);

        return llv;
    }

    public LinearLayout tampilbaris(String[] data) {
        LinearLayout.LayoutParams lpp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lpp.setMargins(1, 1, 1, 1);

        LinearLayout llv = new LinearLayout(this);
        llv.setOrientation(LinearLayout.VERTICAL);

        LinearLayout llh = new LinearLayout(this);
        llh.setOrientation(LinearLayout.HORIZONTAL);
        llv.addView(llh);

        for (int i = 0; i < data.length; i++) {
            TextView textview = new TextView(this);
            int maxLength = 25;
            if (maxLength > String.valueOf(data[i]).length()) {
                maxLength = String.valueOf(data[i]).length();
            }
            textview.setText(data[i].substring(0, maxLength));
            textview.setWidth(250);
            textview.setBackgroundColor(Color.parseColor("#FDFDFD"));
            textview.setLayoutParams(lpp);
            textview.setPadding(5, 2, 2, 2);
            llh.addView(textview);
        }

        llv.setBackgroundColor(Color.parseColor("#A1A1A1"));
        llv.setLayoutParams(lpp);
        llv.setPadding(1, 1, 1, 1);

        return llv;
    }

    public LinearLayout tampilbaris(double[] data) {
        LinearLayout.LayoutParams lpp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lpp.setMargins(1, 1, 1, 1);

        LinearLayout llv = new LinearLayout(this);
        llv.setOrientation(LinearLayout.VERTICAL);

        LinearLayout llh = new LinearLayout(this);
        llh.setOrientation(LinearLayout.HORIZONTAL);
        llv.addView(llh);

        for (int i = 0; i < data.length; i++) {
            TextView textview = new TextView(this);
            int maxLength = 8;
            if (maxLength > String.valueOf(data[i]).length()) {
                maxLength = String.valueOf(data[i]).length();
            }
            textview.setText(String.valueOf(data[i]).substring(0, maxLength));
            textview.setWidth(250);
            textview.setGravity(Gravity.CENTER);
            textview.setBackgroundColor(Color.parseColor("#FDFDFD"));
            textview.setLayoutParams(lpp);
            textview.setPadding(2, 2, 2, 2);
            llh.addView(textview);
        }

        llv.setBackgroundColor(Color.parseColor("#A1A1A1"));
        llv.setLayoutParams(lpp);
        llv.setPadding(1, 1, 1, 1);

        return llv;
    }

    public LinearLayout tampilkolom(String[] data) {
        LinearLayout.LayoutParams lpp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lpp.setMargins(1, 1, 1, 1);

        LinearLayout llv = new LinearLayout(this);
        llv.setOrientation(LinearLayout.VERTICAL);
        for (int i = 0; i < data.length; i++) {
            LinearLayout llh = new LinearLayout(this);
            llh.setOrientation(LinearLayout.HORIZONTAL);
            llv.addView(llh);

            TextView textview = new TextView(this);
            int maxLength = 50;
            if (maxLength > data[i].length()) {
                maxLength = data[i].length();
            }
            textview.setText(data[i].substring(0, maxLength));
            textview.setWidth(500);
            textview.setBackgroundColor(Color.parseColor("#FDFDFD"));
            textview.setLayoutParams(lpp);
            textview.setPadding(5, 2, 2, 2);
            llh.addView(textview);
        }

        llv.setBackgroundColor(Color.parseColor("#A1A1A1"));
        llv.setLayoutParams(lpp);
        llv.setPadding(1, 1, 1, 1);

        return llv;
    }

    public LinearLayout tampilkolom(double[] data) {
        LinearLayout.LayoutParams lpp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lpp.setMargins(1, 1, 1, 1);

        LinearLayout llv = new LinearLayout(this);
        llv.setOrientation(LinearLayout.VERTICAL);
        for (int i = 0; i < data.length; i++) {
            LinearLayout llh = new LinearLayout(this);
            llh.setOrientation(LinearLayout.HORIZONTAL);
            llv.addView(llh);

            TextView textview = new TextView(this);
            int maxLength = 8;
            if (maxLength > String.valueOf(data[i]).length()) {
                maxLength = String.valueOf(data[i]).length();
            }
            textview.setText(String.valueOf(data[i]).substring(0, maxLength));
            textview.setWidth(250);
            textview.setGravity(Gravity.CENTER);
            textview.setBackgroundColor(Color.parseColor("#FDFDFD"));
            textview.setLayoutParams(lpp);
            textview.setPadding(2, 2, 2, 2);
            llh.addView(textview);
        }

        llv.setBackgroundColor(Color.parseColor("#A1A1A1"));
        llv.setLayoutParams(lpp);
        llv.setPadding(1, 1, 1, 1);

        return llv;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analisa_saw);
        dbHelper = new SQLHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM alternatif", null);
        alternatif = new String[cursor.getCount()];
        id_alternatif = new String[cursor.getCount()];
        lokasi_alternatif = new String[cursor.getCount()];
        biaya_alternatif = new String[cursor.getCount()];
        fasilitas_alternatif = new String[cursor.getCount()];
        transportasi_alternatif = new String[cursor.getCount()];
        jarak_alternatif = new String[cursor.getCount()];

        cursor.moveToFirst();
        for (int cc = 0; cc < cursor.getCount(); cc++) {
            cursor.moveToPosition(cc);
            alternatif[cc] = cursor.getString(1).toString();
            id_alternatif[cc] = cursor.getString(0).toString();
            lokasi_alternatif[cc] = cursor.getString(2).toString();
            biaya_alternatif[cc] = cursor.getString(3).toString();
            fasilitas_alternatif[cc] = cursor.getString(4).toString();
            transportasi_alternatif[cc] = cursor.getString(5).toString();
            jarak_alternatif[cc] = cursor.getString(6).toString();
        }

        cursor = db.rawQuery("SELECT * FROM kriteria", null);
        kriteria = new String[cursor.getCount()];
        kepentingan = new double[cursor.getCount()];
        costbenefit = new String[cursor.getCount()];
        id_kriteria = new String[cursor.getCount()];
        cursor.moveToFirst();
        for (int cc = 0; cc < cursor.getCount(); cc++) {
            cursor.moveToPosition(cc);
            kriteria[cc] = cursor.getString(1).toString();
            kepentingan[cc] = Double.parseDouble(cursor.getString(2).toString());
            costbenefit[cc] = cursor.getString(3).toString();
            id_kriteria[cc] = cursor.getString(0).toString();
        }

        alternatifkriteria = new double[alternatif.length][kriteria.length];
        for (int i = 0; i < alternatif.length; i++) {
            for (int j = 0; j < kriteria.length; j++) {
                cursor = db.rawQuery("SELECT * FROM alternatif_kriteria WHERE id_alternatif = '" + id_alternatif[i] + "' AND id_kriteria = '" + id_kriteria[j] + "'", null);
                cursor.moveToFirst();
                if (cursor.getCount() > 0) {
                    cursor.moveToPosition(0);
                    alternatifkriteria[i][j] = Double.parseDouble(cursor.getString(3).toString());
                }
            }
        }

        double[] pembagi = new double[kriteria.length];

        for (int i = 0; i < kriteria.length; i++) {
            pembagi[i] = 0;
            if (costbenefit[i].equalsIgnoreCase("cost") == true) {
                for (int j = 0; j < alternatif.length; j++) {
                    if (j == 0) {
                        pembagi[i] = alternatifkriteria[j][i];
                    } else {
                        if (pembagi[i] > alternatifkriteria[j][i]) {
                            pembagi[i] = alternatifkriteria[j][i];
                        }
                    }
                }
            } else {
                for (int j = 0; j < alternatif.length; j++) {
                    if (j == 0) {
                        pembagi[i] = alternatifkriteria[j][i];
                    } else {
                        if (pembagi[i] < alternatifkriteria[j][i]) {
                            pembagi[i] = alternatifkriteria[j][i];
                        }
                    }
                }
            }
        }

        double[][] normalisasi = new double[alternatif.length][kriteria.length];
        for (int i = 0; i < alternatif.length; i++) {
            for (int j = 0; j < kriteria.length; j++) {
                if (costbenefit[j].equalsIgnoreCase("cost") == true) {
                    normalisasi[i][j] = pembagi[j] / alternatifkriteria[i][j];
                } else {
                    normalisasi[i][j] = alternatifkriteria[i][j] / pembagi[j];
                }
            }
        }

        double[] hasil = new double[alternatif.length];
        for (int i = 0; i < alternatif.length; i++) {
            hasil[i] = 0;
            for (int j = 0; j < kriteria.length; j++) {
                hasil[i] = hasil[i] + (normalisasi[i][j] * kepentingan[j]);
            }
        }

        String[] alternatifrangking = new String[alternatif.length];
        String[] alternatiflokasi = new String[alternatif.length];
        double[] hasilrangking = new double[alternatif.length];
        for (int i = 0; i < alternatif.length; i++) {
            hasilrangking[i] = hasil[i];
            alternatifrangking[i] = alternatif[i];
        }

        for (int i = 0; i < alternatif.length; i++) {
            for (int j = i; j < alternatif.length; j++) {
                if (hasilrangking[j] > hasilrangking[i]) {
                    double tmphasil = hasilrangking[i];
                    String tmpalternatif = alternatifrangking[i];
                    String tmplokasi = alternatiflokasi[i];
                    hasilrangking[i] = hasilrangking[j];
                    alternatifrangking[i] = alternatifrangking[j];
                    alternatiflokasi[i] = alternatiflokasi[j];
                    hasilrangking[j] = tmphasil;
                    alternatifrangking[j] = tmpalternatif;
                    alternatiflokasi[j] = tmplokasi;
                }
            }
        }

        LinearLayout llg = (LinearLayout) findViewById(R.id.LLGroup);
        LinearLayout llr = (LinearLayout) findViewById(R.id.llRanking);
        LinearLayout.LayoutParams lppp = new LinearLayout.LayoutParams((int) 0, LayoutParams.WRAP_CONTENT, (float) 0.5);
        lppp.setMargins(1, 1, 1, 1);

        for (int i = 0; i < hasilrangking.length; i++) {
            LinearLayout llh = new LinearLayout(this);
            llh.setOrientation(LinearLayout.HORIZONTAL);
            llr.addView(llh);

            TextView textviewAlternatifRanking = new TextView(this);
            int maxLength = 13;
            if (maxLength > alternatifrangking[i].length()) {
                maxLength = alternatifrangking[i].length();
            }
            textviewAlternatifRanking.setText(alternatifrangking[i].substring(0, maxLength));
            textviewAlternatifRanking.setBackgroundColor(Color.parseColor("#FDFDFD"));
            textviewAlternatifRanking.setLayoutParams(lppp);
            textviewAlternatifRanking.setPadding(10, 2, 10, 2);
            llh.addView(textviewAlternatifRanking);

            TextView textviewHasilRanking = new TextView(this);
            maxLength = 10;
            if (maxLength > String.valueOf(hasilrangking[i]).length()) {
                maxLength = String.valueOf(hasilrangking[i]).length();
            }
            textviewHasilRanking.setText(String.valueOf(hasilrangking[i]).substring(0, maxLength));
            textviewHasilRanking.setGravity(Gravity.CENTER);
            textviewHasilRanking.setBackgroundColor(Color.parseColor("#FDFDFD"));
            textviewHasilRanking.setLayoutParams(lppp);
            textviewHasilRanking.setPadding(2, 2, 2, 2);
            llh.addView(textviewHasilRanking);
        }

        LinearLayout llb = new LinearLayout(this);
        llb.setOrientation(LinearLayout.VERTICAL);
        TextView tvb = (TextView) findViewById(R.id.TVBest);
        tvb.setText("Wisata Terbaik = " + alternatifrangking[0] + " (" + Double.valueOf(hasilrangking[0]) + ")");
        llg.addView(llb);
        final LinearLayout llhasil = new LinearLayout(this);
        Button btnshow = (Button) findViewById(R.id.bShow);
        btnshow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                llhasil.setVisibility(View.VISIBLE);
            }
        });

        LinearLayout ll = (LinearLayout) findViewById(R.id.llAnalisa);

        llhasil.setOrientation(LinearLayout.VERTICAL);
        llhasil.setVisibility(View.GONE);
        ll.addView(llhasil);

        TextView tvRatingAlternatif = new TextView(this);
        tvRatingAlternatif.setText("Nilai Rating :");
        tvRatingAlternatif.setTypeface(Typeface.DEFAULT_BOLD);
        tvRatingAlternatif.setPadding(5, 2, 5, 2);

        LinearLayout llh = new LinearLayout(this);
        llh.setOrientation(LinearLayout.HORIZONTAL);

        LinearLayout llalternatifkriteria = tampiltabel(alternatifkriteria);
        llalternatifkriteria.setOrientation(LinearLayout.VERTICAL);

        LinearLayout llalternatif = tampilkolom(alternatif);
        llalternatif.setOrientation(LinearLayout.VERTICAL);

        llhasil.addView(llh);

        TextView tvkriteria = new TextView(this);
        tvkriteria.setText("Kriteria :");
        tvkriteria.setPadding(5, 2, 5, 2);
        tvkriteria.setTypeface(Typeface.DEFAULT_BOLD);
        LinearLayout llkriteria = tampilbaris(kriteria);
        llkriteria.setOrientation(LinearLayout.VERTICAL);

        TextView tvkepentingan = new TextView(this);
        tvkepentingan.setText("Bobot :");
        tvkepentingan.setPadding(5, 2, 5, 2);
        tvkepentingan.setTypeface(Typeface.DEFAULT_BOLD);
        LinearLayout llkepentingan = tampilbaris(kepentingan);
        llkepentingan.setOrientation(LinearLayout.VERTICAL);

        TextView tvcostbenefit = new TextView(this);
        tvcostbenefit.setText("Atribut :");
        tvcostbenefit.setPadding(5, 2, 5, 2);
        tvcostbenefit.setTypeface(Typeface.DEFAULT_BOLD);
        LinearLayout llcostbenefit = tampilbaris(costbenefit);
        llcostbenefit.setOrientation(LinearLayout.VERTICAL);

        TextView tvpembagi = new TextView(this);
        tvpembagi.setText("Pembagi :");
        tvpembagi.setPadding(5, 2, 5, 2);
        tvpembagi.setTypeface(Typeface.DEFAULT_BOLD);
        LinearLayout llpembagi = tampilbaris(pembagi);
        llpembagi.setOrientation(LinearLayout.VERTICAL);

        TextView tvnormalisasi = new TextView(this);
        tvnormalisasi.setText("Normalisasi :");
        tvnormalisasi.setPadding(5, 2, 5, 2);
        tvnormalisasi.setTypeface(Typeface.DEFAULT_BOLD);

        LinearLayout llhn = new LinearLayout(this);
        llhn.setOrientation(LinearLayout.HORIZONTAL);

        LinearLayout llnormalisasi = tampiltabel(normalisasi);
        llnormalisasi.setOrientation(LinearLayout.VERTICAL);

        LinearLayout llan = tampilkolom(alternatif);
        llan.setOrientation(LinearLayout.VERTICAL);

        TextView tvhasil = new TextView(this);
        tvhasil.setText("Rangking :");
        tvhasil.setPadding(5, 2, 5, 2);
        tvhasil.setTypeface(Typeface.DEFAULT_BOLD);

        LinearLayout llendh = new LinearLayout(this);
        llendh.setOrientation(LinearLayout.HORIZONTAL);

        LinearLayout llrs = new LinearLayout(this);
        llrs.setOrientation(LinearLayout.HORIZONTAL);

        LinearLayout llend1 = new LinearLayout(this);
        llend1.setOrientation(LinearLayout.VERTICAL);
        llrs.addView(llend1);
        LinearLayout llend2 = new LinearLayout(this);
        llend2.setOrientation(LinearLayout.VERTICAL);
        llrs.addView(llend2);
        LinearLayout llend3 = new LinearLayout(this);
        llend3.setOrientation(LinearLayout.VERTICAL);
        llrs.addView(llend3);
        LinearLayout llend4 = new LinearLayout(this);
        llend4.setOrientation(LinearLayout.VERTICAL);
        llrs.addView(llend4);
        LinearLayout llend5 = new LinearLayout(this);
        llend5.setOrientation(LinearLayout.VERTICAL);
        llrs.addView(llend5);
        LinearLayout llend6 = new LinearLayout(this);
        llend6.setOrientation(LinearLayout.VERTICAL);
        llrs.addView(llend6);
        LinearLayout llend7 = new LinearLayout(this);
        llend7.setOrientation(LinearLayout.VERTICAL);
        llrs.addView(llend7);

        LinearLayout llhasil2 = tampilkolom(hasil);
        llhasil2.setOrientation(LinearLayout.VERTICAL);
        TextView tvnilai = new TextView(this);
        tvnilai.setGravity(Gravity.CENTER);
        tvnilai.setText("Nilai");
        tvnilai.setWidth(150);
        tvnilai.setPadding(5, 2, 5, 2);
        tvnilai.setTypeface(Typeface.DEFAULT_BOLD);
        tvnilai.setTextColor(Color.parseColor("#9C27B0"));

        LinearLayout llalternatifranking = tampilkolom(alternatif);
        llalternatifranking.setOrientation(LinearLayout.VERTICAL);
        TextView tvtempat = new TextView(this);
        tvtempat.setGravity(Gravity.CENTER);
        tvtempat.setText("Wisata");
        tvtempat.setWidth(300);
        tvtempat.setPadding(5, 2, 5, 2);
        tvtempat.setTypeface(Typeface.DEFAULT_BOLD);
        tvtempat.setTextColor(Color.parseColor("#9C27B0"));
        llend2.addView(tvtempat);
        llend2.addView(llalternatifranking);

        LinearLayout llalternatiflokasi = tampilkolom(lokasi_alternatif);
        llalternatiflokasi.setOrientation(LinearLayout.VERTICAL);
        TextView tvlokasi = new TextView(this);
        tvlokasi.setGravity(Gravity.CENTER);
        tvlokasi.setText("Lokasi");
        tvlokasi.setWidth(500);
        tvlokasi.setPadding(5, 2, 5, 2);
        tvlokasi.setTypeface(Typeface.DEFAULT_BOLD);
        tvlokasi.setTextColor(Color.parseColor("#9C27B0"));
        llend3.addView(tvlokasi);
        llend3.addView(llalternatiflokasi);

        LinearLayout llalternatifbiaya = tampilkolom(biaya_alternatif);
        llalternatifbiaya.setOrientation(LinearLayout.VERTICAL);
        TextView tvbiaya = new TextView(this);
        tvbiaya.setGravity(Gravity.CENTER);
        tvbiaya.setText("Biaya");
        tvbiaya.setWidth(500);
        tvbiaya.setPadding(5, 2, 5, 2);
        tvbiaya.setTypeface(Typeface.DEFAULT_BOLD);
        tvbiaya.setTextColor(Color.parseColor("#9C27B0"));
        llend4.addView(tvbiaya);
        llend4.addView(llalternatifbiaya);

        LinearLayout llalternatiffasilitas = tampilkolom(fasilitas_alternatif);
        llalternatiffasilitas.setOrientation(LinearLayout.VERTICAL);
        TextView tvfasilitas = new TextView(this);
        tvfasilitas.setGravity(Gravity.CENTER);
        tvfasilitas.setText("Fasilitas");
        tvfasilitas.setWidth(500);
        tvfasilitas.setPadding(5, 2, 5, 2);
        tvfasilitas.setTypeface(Typeface.DEFAULT_BOLD);
        tvfasilitas.setTextColor(Color.parseColor("#9C27B0"));
        llend5.addView(tvfasilitas);
        llend5.addView(llalternatiffasilitas);

        LinearLayout llalternatiftransportasi = tampilkolom(transportasi_alternatif);
        llalternatiftransportasi.setOrientation(LinearLayout.VERTICAL);
        TextView tvtransportasi = new TextView(this);
        tvtransportasi.setGravity(Gravity.CENTER);
        tvtransportasi.setText("Transportasi");
        tvtransportasi.setWidth(400);
        tvtransportasi.setPadding(5, 2, 5, 2);
        tvtransportasi.setTypeface(Typeface.DEFAULT_BOLD);
        tvtransportasi.setTextColor(Color.parseColor("#9C27B0"));
        llend6.addView(tvtransportasi);
        llend6.addView(llalternatiftransportasi);

        LinearLayout llalternatifjarak = tampilkolom(jarak_alternatif);
        llalternatifjarak.setOrientation(LinearLayout.VERTICAL);
        TextView tvjarak = new TextView(this);
        tvjarak.setGravity(Gravity.CENTER);
        tvjarak.setText("Jarak");
        tvjarak.setWidth(750);
        tvjarak.setPadding(5, 2, 5, 2);
        tvjarak.setTypeface(Typeface.DEFAULT_BOLD);
        tvjarak.setTextColor(Color.parseColor("#9C27B0"));
        llend7.addView(tvjarak);
        llend7.addView(llalternatifjarak);

        llhasil.addView(llrs);

        TextView tvalternatifterbaik = new TextView(this);
        tvalternatifterbaik.setText("Wisata Terbaik : " + alternatifrangking[0] + " dengan nilai sebesar " + Double.valueOf(hasilrangking[0]));
        tvalternatifterbaik.setPadding(5, 5, 5, 5);
        tvalternatifterbaik.setTypeface(Typeface.DEFAULT_BOLD);
    }

}