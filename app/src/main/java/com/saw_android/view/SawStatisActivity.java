package com.saw_android.view;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.saw_android.R;

public class SawStatisActivity extends AppCompatActivity {

    String[] alternatif = {"", "", "", ""};
    String[] kriteria = {"", "", "", "", "", ""};
    String[] costbenefit = {"", "", "", "", "", ""};
    double[] kepentingan = {};
    double[][] alternatifkriteria = {
            {},
            {},
            {},
            {}
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saw_statis);

        Display display = getWindowManager().getDefaultDisplay();

        ScrollView sv = (ScrollView) findViewById(R.id.svSAW);
        LinearLayout l = new LinearLayout(this);
        l.setOrientation(LinearLayout.VERTICAL);
        sv.addView(l);
        HorizontalScrollView hv = new HorizontalScrollView(this);
        ViewGroup.LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        hv.setLayoutParams(lp);
        l.addView(hv);

        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);
        ll.setMinimumWidth(700);

        TextView tvhasilrankingfinal = new TextView(this);
        tvhasilrankingfinal.setText("Hasil Perhitungan :");
        tvhasilrankingfinal.setTextColor(Color.parseColor("#0000ff"));
        tvhasilrankingfinal.setTypeface(Typeface.DEFAULT_BOLD);
        ViewGroup.LayoutParams lpp = new LayoutParams(display.getWidth(), LayoutParams.WRAP_CONTENT);
        tvhasilrankingfinal.setLayoutParams(lpp);
        ll.addView(tvhasilrankingfinal);
        LinearLayout llalternatifrankingfinal = new LinearLayout(this);

        LinearLayout.LayoutParams lppp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lppp.setMargins(1, 1, 1, 1);

        llalternatifrankingfinal.setOrientation(LinearLayout.VERTICAL);

        LinearLayout llhjudul = new LinearLayout(this);
        llhjudul.setOrientation(LinearLayout.HORIZONTAL);
        llalternatifrankingfinal.addView(llhjudul);

        TextView textviewAlternatifJudul = new TextView(this);
        textviewAlternatifJudul.setText("Alternatif");
        textviewAlternatifJudul.setWidth(100);
        textviewAlternatifJudul.setTypeface(Typeface.DEFAULT_BOLD);
        textviewAlternatifJudul.setBackgroundColor(Color.parseColor("#ffffff"));
        textviewAlternatifJudul.setLayoutParams(lppp);
        textviewAlternatifJudul.setPadding(2, 2, 2, 2);
        llhjudul.addView(textviewAlternatifJudul);

        TextView textviewHasilJudul = new TextView(this);
        textviewHasilJudul.setText("Nilai");
        textviewHasilJudul.setWidth(100);
        textviewHasilJudul.setTypeface(Typeface.DEFAULT_BOLD);
        textviewHasilJudul.setBackgroundColor(Color.parseColor("#ffffff"));
        textviewHasilJudul.setLayoutParams(lppp);
        textviewHasilJudul.setPadding(2, 2, 2, 2);
        llhjudul.addView(textviewHasilJudul);

        llalternatifrankingfinal.setBackgroundColor(Color.parseColor("#0000ff"));
        llalternatifrankingfinal.setLayoutParams(lppp);
        llalternatifrankingfinal.setPadding(1, 1, 1, 1);

        llalternatifrankingfinal.setOrientation(LinearLayout.VERTICAL);
        ll.addView(llalternatifrankingfinal);

        final LinearLayout llhasil = new LinearLayout(this);

        Button btnshow = new Button(this);
        btnshow.setText("Perhitungan");
        btnshow.setBackgroundColor(Color.parseColor("#0000ff"));
        btnshow.setTypeface(Typeface.DEFAULT_BOLD);
        btnshow.setTextColor(Color.parseColor("#0000ff"));
        btnshow.setBackgroundColor(Color.parseColor("#01fecf"));
        btnshow.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        btnshow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                llhasil.setVisibility(View.VISIBLE);
            }
        });

        ll.addView(btnshow);
        llhasil.setOrientation(LinearLayout.VERTICAL);
        llhasil.setVisibility(View.GONE);
        ll.addView(llhasil);
        hv.addView(ll);
    }
}