package com.saw_android.view;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.saw_android.R;
import com.saw_android.database.SQLHelper;

public class AddKriteriaActivity extends AppCompatActivity {

    protected Cursor cursor;
    SQLHelper dbHelper;

    Button btnaddkriteriasave;
    TextInputEditText edaddkriterianama;
    TextInputEditText edaddkriteriakepentingan;
    Spinner spnaddkriteriacostbenefit;

    String[] array_spncostbenefit = {"cost", "benefit"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_kriteria);

        dbHelper = new SQLHelper(this);

        edaddkriterianama = (TextInputEditText) findViewById(R.id.edaddkriterianama);
        edaddkriteriakepentingan = (TextInputEditText) findViewById(R.id.edaddkriteriakepentingan);
        spnaddkriteriacostbenefit = (Spinner) findViewById(R.id.spnaddkriteriacostbenefit);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, array_spncostbenefit);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnaddkriteriacostbenefit.setAdapter(adapter);

        btnaddkriteriasave = (Button) findViewById(R.id.btnaddkriteriasave);
        btnaddkriteriasave.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                String cost_benefit = "";
                if (spnaddkriteriacostbenefit.getSelectedItemPosition() == 0) {
                    cost_benefit = "cost";
                } else {
                    cost_benefit = "benefit";
                }
                db.execSQL("insert into kriteria(nama_kriteria, kepentingan, cost_benefit) values('" +
                        edaddkriterianama.getText().toString() + "','" +
                        edaddkriteriakepentingan.getText().toString() + "','" +
                        cost_benefit + "')");
                Toast.makeText(getApplicationContext(), "Berhasil menambahkan data.", Toast.LENGTH_LONG).show();
                KriteriaActivity.obj.RefreshList();
                finish();
            }

        });

    }

}
