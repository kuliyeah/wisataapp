package com.saw_android.view;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;

import com.saw_android.R;

public class AdminActivity extends AppCompatActivity {

    Button btnadminalternatif;
    Button btnadminkriteria;
    Button btnadminalternatifkriteria;
    Button btnadminpassword;
    Button btnadminback;

    public static String userlogin = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        btnadminalternatif = (Button) findViewById(R.id.btnadminalternatif);
        btnadminkriteria = (Button) findViewById(R.id.btnadminkriteria);
        btnadminalternatifkriteria = (Button) findViewById(R.id.btnadminalternatifkriteria);
        btnadminpassword = (Button) findViewById(R.id.btnadminpassword);
        btnadminback = (Button) findViewById(R.id.btnadminback);

        btnadminalternatif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(AdminActivity.this, AlternatifActivity.class);
                startActivity(i);
            }
        });

        btnadminkriteria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(AdminActivity.this, KriteriaActivity.class);
                startActivity(i);
            }
        });

        btnadminalternatifkriteria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(AdminActivity.this, AlternatifKriteriaActivity.class);
                startActivity(i);
            }
        });

        btnadminpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(AdminActivity.this, ChangePasswordActivity.class);
                startActivity(i);
            }
        });

        btnadminback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                finish();
            }
        });

    }

}
