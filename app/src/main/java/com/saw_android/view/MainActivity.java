package com.saw_android.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import androidx.cardview.widget.CardView;

import com.saw_android.R;

public class MainActivity extends Activity {
    CardView btnmainanalisa;
    CardView btnmainspk;
    CardView btnmainadmin;
    CardView btnmainmaps;
    CardView btnmainexit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        btnmainanalisa = (CardView) findViewById(R.id.btnmainanalisa);
        btnmainspk = (CardView) findViewById(R.id.btnmainspk);
        btnmainadmin = (CardView) findViewById(R.id.btnmainadmin);
        btnmainmaps = (CardView) findViewById(R.id.btnmainmap);
        btnmainexit = (CardView) findViewById(R.id.btnmainexit);

        btnmainanalisa.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(MainActivity.this, AnalisaSawActivity.class);
                startActivity(i);
            }
        });

        btnmainspk.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(MainActivity.this, SawStatisActivity.class);
                startActivity(i);
            }
        });

        btnmainadmin.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        btnmainmaps.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(MainActivity.this, MapsOnlyActivity.class);
                startActivity(i);
            }
        });

        btnmainexit.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                finish();
            }
        });
    }

}
