package com.saw_android.view;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.saw_android.R;
import com.saw_android.database.SQLHelper;

public class AddAlternatifActivity extends AppCompatActivity {

    protected Cursor cursor;
    SQLHelper dbHelper;

    Button btnaddalternatifsave;
    TextInputEditText edaddalternatifnama;
    TextInputEditText edaddalternatiflokasi;
    TextInputEditText edaddalternatifbiaya;
    TextInputEditText edaddalternatiffasilitas;
    TextInputEditText edaddalternatiftransportasi;
    TextInputEditText edaddalternatifjarak;
    TextInputEditText edaddalternatiflat;
    TextInputEditText edaddalternatiflng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_alternatif);

        dbHelper = new SQLHelper(this);

        edaddalternatifnama = (TextInputEditText) findViewById(R.id.edaddalternatifnama);
        edaddalternatiflokasi = (TextInputEditText) findViewById(R.id.edaddalternatiflokasi);
        edaddalternatifbiaya = (TextInputEditText) findViewById(R.id.edaddalternatifbiaya);
        edaddalternatiffasilitas = (TextInputEditText) findViewById(R.id.edaddalternatiffasilitas);
        edaddalternatiftransportasi = (TextInputEditText) findViewById(R.id.edaddalternatiftransportasi);
        edaddalternatifjarak = (TextInputEditText) findViewById(R.id.edaddalternatifjarak);
        edaddalternatiflat = (TextInputEditText) findViewById(R.id.edaddalternatiflatitude);
        edaddalternatiflng = (TextInputEditText) findViewById(R.id.edaddalternatiflongtitude);

        edaddalternatiflat.setEnabled(false);
        edaddalternatiflng.setEnabled(false);

        edaddalternatifnama.setText(getIntent().getStringExtra("nama_alternatif"));
        edaddalternatiflokasi.setText(getIntent().getStringExtra("lokasi"));
        edaddalternatiflat.setText(getIntent().getStringExtra("latitude"));
        edaddalternatiflng.setText(getIntent().getStringExtra("longitude"));

        btnaddalternatifsave = (Button) findViewById(R.id.btnaddalternatifsave);
        btnaddalternatifsave.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.execSQL("insert into alternatif(nama_alternatif, lokasi, biaya, fasilitas, transportasi, jarak, latitude, longitude) values('" +
                        edaddalternatifnama.getText().toString() + "','" +
                        edaddalternatiflokasi.getText().toString() + "','" +
                        edaddalternatifbiaya.getText().toString() + "','" +
                        edaddalternatiffasilitas.getText().toString() + "','" +
                        edaddalternatiftransportasi.getText() + "','" +
                        edaddalternatifjarak.getText().toString() + "','" +
                        edaddalternatiflat.getText() + "','" +
                        edaddalternatiflng.getText()+ "')");
                Toast.makeText(getApplicationContext(), "Berhasil menambahkan data.", Toast.LENGTH_LONG).show();
                AlternatifActivity.obj.RefreshList();
                Intent i = new Intent(getApplicationContext(), AlternatifActivity.class);
                startActivity(i);
                finish();
            }
        });
    }
}