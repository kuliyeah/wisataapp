package com.saw_android.view;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.saw_android.R;
import com.saw_android.adapter.AlternatifAdapter;
import com.saw_android.database.SQLHelper;
import com.saw_android.model.Alternatif;

import java.util.ArrayList;
import java.util.HashMap;

public class AlternatifActivity extends AppCompatActivity {
    String[] array_id_alternatif;
    String[] array_nama_alternatif;
    String[] array_lokasi;
    String[] array_biaya;
    String[] array_fasilitas;
    String[] array_transportasi;
    String[] array_jarak;
    String[] array_alternatif;
    String[] array_lat;
    String[] array_lng;
    ListView listalternatif;

    FloatingActionButton btnalternatifadd;

    protected Cursor cursor;
    Alternatif wisata;
    SQLHelper dbHelper;
    public static AlternatifActivity obj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alternatif);
        obj = this;
        dbHelper = new SQLHelper(this);
        btnalternatifadd = (FloatingActionButton) findViewById(R.id.btnalternatifadd);
        btnalternatifadd.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(AlternatifActivity.this, MapsActivity.class);
                startActivity(i);
            }
        });
        RefreshList();
    }

    public void RefreshList() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM alternatif ORDER BY id_alternatif DESC", null);

        array_id_alternatif = new String[cursor.getCount()];
        array_nama_alternatif = new String[cursor.getCount()];
        array_lokasi = new String[cursor.getCount()];
        array_biaya = new String[cursor.getCount()];
        array_fasilitas = new String[cursor.getCount()];
        array_transportasi = new String[cursor.getCount()];
        array_jarak = new String[cursor.getCount()];
        array_lat = new String[cursor.getCount()];
        array_lng = new String[cursor.getCount()];
        array_alternatif = new String[cursor.getCount()];
        cursor.moveToFirst();

        for (int cc = 0; cc < cursor.getCount(); cc++) {
            cursor.moveToPosition(cc);
            array_id_alternatif[cc] = cursor.getString(0).toString();
            array_nama_alternatif[cc] = cursor.getString(1).toString();
            array_lokasi[cc] = cursor.getString(2).toString();
            array_biaya[cc] = cursor.getString(3).toString();
            array_fasilitas[cc] = cursor.getString(4).toString();
            array_transportasi[cc] = cursor.getString(5).toString();
            array_jarak[cc] = cursor.getString(6).toString();
            array_lat[cc] = String.valueOf(cursor.getDouble(7));
            array_lng[cc] = String.valueOf(cursor.getDouble(8));
            array_alternatif[cc] = cursor.getString(0).toString() + ". " + cursor.getString(1).toString() + "\n" + cursor.getString(2).toString();
        }

        wisata = new Alternatif(this);
        ArrayList<HashMap<String, String>> trxList = wisata.getList();
        AlternatifAdapter adapter = new AlternatifAdapter(this, trxList);

        listalternatif = (ListView) findViewById(R.id.listalternatif);
        listalternatif.setAdapter(adapter);
        listalternatif.setSelected(true);
        listalternatif.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                final int posisi = arg2;
                final CharSequence[] dialogitem = {"Edit", "Delete"};
                AlertDialog.Builder builder = new AlertDialog.Builder(AlternatifActivity.this);
                builder.setTitle("Pilih ?");
                builder.setItems(dialogitem, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        switch (item) {
                            case 0:
                                Intent i = new Intent(getApplicationContext(), EditAlternatifActivity.class);
                                i.putExtra("id_alternatif", array_id_alternatif[posisi]);
                                i.putExtra("nama_alternatif", array_nama_alternatif[posisi]);
                                i.putExtra("lokasi", array_lokasi[posisi]);
                                i.putExtra("biaya", array_biaya[posisi]);
                                i.putExtra("fasilitas", array_fasilitas[posisi]);
                                i.putExtra("transportasi", array_transportasi[posisi]);
                                i.putExtra("jarak", array_jarak[posisi]);
                                i.putExtra("latitude", array_lat[posisi]);
                                i.putExtra("longitude", array_lng[posisi]);
                                startActivity(i);
                                break;
                            case 1:
                                SQLiteDatabase db = dbHelper.getWritableDatabase();
                                db.execSQL("delete from alternatif where id_alternatif = '" + array_id_alternatif[posisi] + "'");
                                RefreshList();
                                break;
                        }
                    }
                });
                builder.create().show();
            }
        });
        ((ArrayAdapter) listalternatif.getAdapter()).notifyDataSetInvalidated();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_alternatif, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_alternatif_add:
                Intent i = new Intent(AlternatifActivity.this, AddAlternatifActivity.class);
                startActivity(i);
                return true;
            case R.id.menu_alternatif_refresh:
                RefreshList();
                return true;
            case R.id.menu_alternatif_exit:
                finish();
                return true;
        }
        return false;
    }

}
