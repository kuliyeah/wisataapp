package com.saw_android.view;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.saw_android.R;
import com.saw_android.database.SQLHelper;

public class ChangePasswordActivity extends AppCompatActivity {

    TextInputEditText edpasswordusername;
    TextInputEditText edpasswordold;
    TextInputEditText edpasswordnew;
    TextInputEditText edpasswordconfirm;
    Button btnpasswordsave;
    protected Cursor cursor;
    SQLHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        dbHelper = new SQLHelper(this);

        edpasswordusername = (TextInputEditText) findViewById(R.id.edpasswordusername);
        edpasswordold = (TextInputEditText) findViewById(R.id.edpasswordold);
        edpasswordnew = (TextInputEditText) findViewById(R.id.edpasswordnew);
        edpasswordconfirm = (TextInputEditText) findViewById(R.id.edpasswordconfirm);

        edpasswordusername.setText(AdminActivity.userlogin);
        edpasswordusername.setEnabled(false);

        btnpasswordsave = (Button) findViewById(R.id.btnpasswordsave);
        btnpasswordsave.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                SQLiteDatabase db = dbHelper.getReadableDatabase();
                cursor = db.rawQuery("SELECT * FROM login WHERE username = '" + edpasswordusername.getText().toString() + "' AND password = '" + edpasswordold.getText().toString() + "'", null);
                if (cursor.getCount() > 0) {
                    if (edpasswordnew.getText().toString().equals(edpasswordconfirm.getText().toString()) == true) {
                        db = dbHelper.getWritableDatabase();
                        db.execSQL("UPDATE login SET password='" + edpasswordnew.getText().toString() + "' " +
                                " WHERE username = '" + edpasswordusername.getText().toString() + "'");
                        Toast.makeText(getApplicationContext(), "Password Berhasil Diubah", Toast.LENGTH_LONG).show();
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "Password Baru Tidak Sama dengan Konfirmasi Password", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Password Lama Salah", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
