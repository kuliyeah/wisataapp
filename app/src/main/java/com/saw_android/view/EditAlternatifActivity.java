package com.saw_android.view;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.saw_android.R;
import com.saw_android.database.SQLHelper;

public class EditAlternatifActivity extends Activity {

    SQLHelper dbHelper;

    Button btneditalternatifsave;
    EditText ededitalternatifid;
    EditText ededitalternatifnama;
    EditText ededitalternatiflokasi;
    EditText ededitalternatifbiaya;
    EditText ededitalternatiffasilitas;
    EditText ededitalternatiftransportasi;
    EditText ededitalternatifjarak;
    EditText edaddalternatiflatitude;
    EditText edaddalternatiflongtitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_edit_alternatif);
        dbHelper = new SQLHelper(this);

        ededitalternatifid = (EditText) findViewById(R.id.ededitalternatifid);
        ededitalternatifnama = (EditText) findViewById(R.id.ededitalternatifnama);
        ededitalternatiflokasi = (EditText) findViewById(R.id.ededitalternatiflokasi);
        ededitalternatifbiaya = (EditText) findViewById(R.id.ededitalternatifbiaya);
        ededitalternatiffasilitas = (EditText) findViewById(R.id.ededitalternatiffasilitas);
        ededitalternatiftransportasi = (EditText) findViewById(R.id.ededitalternatiftransportasi);
        ededitalternatifjarak = (EditText) findViewById(R.id.ededitalternatifjarak);
        edaddalternatiflatitude = (EditText) findViewById(R.id.edaddalternatiflatitude);
        edaddalternatiflongtitude = (EditText) findViewById(R.id.edaddalternatiflongtitude);

        edaddalternatiflatitude.setEnabled(false);
        edaddalternatiflongtitude.setEnabled(false);

        ededitalternatifid.setText(getIntent().getStringExtra("id_alternatif"));
        ededitalternatifnama.setText(getIntent().getStringExtra("nama_alternatif"));
        ededitalternatiflokasi.setText(getIntent().getStringExtra("lokasi"));
        ededitalternatifbiaya.setText(getIntent().getStringExtra("biaya"));
        ededitalternatiffasilitas.setText(getIntent().getStringExtra("fasilitas"));
        ededitalternatiftransportasi.setText(getIntent().getStringExtra("transportasi"));
        ededitalternatifjarak.setText(getIntent().getStringExtra("jarak"));
        edaddalternatiflatitude.setText(getIntent().getStringExtra("latitude"));
        edaddalternatiflongtitude.setText(getIntent().getStringExtra("longitude"));

        btneditalternatifsave = (Button) findViewById(R.id.btneditalternatifsave);
        btneditalternatifsave.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.execSQL("update alternatif SET nama_alternatif='" + ededitalternatifnama.getText().toString() + "', " +
                        "lokasi='" + ededitalternatiflokasi.getText().toString() + "', " +
                        "biaya='" + ededitalternatifbiaya.getText().toString() + "', " +
                        "fasilitas='" + ededitalternatiffasilitas.getText().toString() + "', " +
                        "transportasi='" + ededitalternatiftransportasi.getText().toString() + "', " +
                        "jarak='" + ededitalternatifjarak.getText().toString() + "' WHERE " +
                        " id_alternatif = '" + getIntent().getStringExtra("id_alternatif") + "'");
                Toast.makeText(getApplicationContext(), "Data Berhasil diperbaharui", Toast.LENGTH_LONG).show();
                AlternatifActivity.obj.RefreshList();
                finish();
            }

        });
    }


}
