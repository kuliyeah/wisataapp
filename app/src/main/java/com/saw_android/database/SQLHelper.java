package com.saw_android.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SQLHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "OneWay.db";
    private static final int DATABASE_VERSION = 1;

    public SQLHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table alternatif( " +
                "id_alternatif integer primary key autoincrement, " +
                "nama_alternatif varchar(200) null, " +
                "lokasi text null, " +
                "biaya text null, " +
                "fasilitas text null, " +
                "transportasi text null, " +
                "jarak text null, " +
                "latitude double," +
                "longitude double);";
        Log.d("Data", "onCreate: " + sql);
        db.execSQL(sql);

        sql = "INSERT INTO alternatif (id_alternatif, nama_alternatif, lokasi, biaya, fasilitas, transportasi, jarak, latitude, longitude) " +
                "VALUES " +
                "('1', 'PANTAI HAMADI', 'Tobati, South Jayapura, Jayapura City, Papua, Indonesia', '20.000', 'Komplit', 'Mobil', '5KM', '-2.5788312', '140.7091912'), " +
                "('2', 'BUKIT JOKOWI', 'Skyland, Entrop, Jayapura Sel., Kota Jayapura, Papua, Indonesia', '5000', 'Kuliner', 'Kendaraan umum dan pribadi', '1 KM', '-2.5884097', '140.6885197'), " +
                "('3', 'PANTAI HOLTEKAMP', 'Holtekamp Beach, Papua, Indonesia', '20000', 'Komplit', 'Kendaraan pribadi dan umum', '39KM', '-2.6243045', '140.783206'), " +
                "('4', 'PANTAI CIBERI', 'Engros, Abepura, Jayapura City, Papua, Indonesia', '250000', 'Komplit', 'Jalur laut dan darat', '36 KM', '-2.5966189', '140.711856'), " +
                "('5', 'KUPANG', 'Kupang, Kupang City, East Nusa Tenggara, Indonesia', 'Gratis', 'Kios', 'Kendaraan pribadi dan umum', '6,3 KM', '-10.1771997', '123.6070329'), " +
                "('6', 'PANTAI PASIR 6', 'Tanjung Ria, North Jayapura, Jayapura City, Papua, Indonesia', '20000', 'Komplit', 'Kendaraan pribadi rute darat', '3KM', '-2.4877879', '140.7117044');";
        db.execSQL(sql);

        sql = "create table kriteria( " +
                "id_kriteria integer primary key autoincrement, " +
                "nama_kriteria varchar(200) null, " +
                "kepentingan double null, " +
                "cost_benefit varchar(50) null);";
        Log.d("Data", "onCreate: " + sql);
        db.execSQL(sql);

        sql = "INSERT INTO kriteria (id_kriteria, nama_kriteria, kepentingan, cost_benefit) " +
                "VALUES " +
                "(1, 'Lokasi', 0.25, 'benefit')," +
                "(2, 'Biaya', 0.25, 'cost')," +
                "(3, 'Fasilitas', 0.15, 'benefit')," +
                "(4, 'Transportasi', 0.10, 'benefit')," +
                "(5, 'Jarak', 0.10, 'benefit')," +
                "(6, 'Keamanan', 0.15, 'benefit');";
        db.execSQL(sql);

        sql = "create table alternatif_kriteria( " +
                "id_alternatif_kriteria integer primary key autoincrement, " +
                "id_alternatif integer null, " +
                "id_kriteria integer null, " +
                "nilai double null);";
        Log.d("Data", "onCreate: " + sql);
        db.execSQL(sql);

        sql = "INSERT INTO alternatif_kriteria (id_alternatif_kriteria, id_alternatif, id_kriteria, nilai) " +
                "VALUES " +
                "('1', '1', '1', '70.0')," +
                "('2', '1', '2', '90.0')," +
                "('3', '1', '3', '60.0')," +
                "('4', '1', '4', '70.0')," +
                "('5', '1', '5', '80.0')," +
                "('6', '1', '6', '90.0')," +
                "('7', '2', '1', '60.0')," +
                "('8', '2', '2', '75.0')," +
                "('9', '2', '3', '75.0')," +
                "('10', '2', '4', '80.0')," +
                "('11', '2', '5', '90.0')," +
                "('12', '2', '6', '65.0')," +
                "('13', '3', '1', '90.0')," +
                "('14', '3', '2', '78.0')," +
                "('15', '3', '3', '90.0')," +
                "('16', '3', '4', '78.0')," +
                "('17', '3', '5', '88.0')," +
                "('18', '3', '6', '98.0')," +
                "('19', '4', '1', '91.0')," +
                "('20', '4', '2', '72.0')," +
                "('21', '4', '3', '94.0')," +
                "('22', '4', '4', '72.0')," +
                "('23', '4', '5', '81.0')," +
                "('24', '4', '6', '92.0')," +
                "('25', '5', '1', '93.0')," +
                "('26', '5', '2', '71.0')," +
                "('27', '5', '3', '90.0')," +
                "('28', '5', '4', '74.0')," +
                "('29', '5', '5', '87.0')," +
                "('30', '5', '6', '96.0')," +
                "('31', '6', '1', '90.0')," +
                "('32', '6', '2', '74.0')," +
                "('33', '6', '3', '92.0')," +
                "('34', '6', '4', '71.0')," +
                "('35', '6', '5', '88.0')," +
                "('36', '6', '6', '97.0');";
        db.execSQL(sql);

        sql = "create table login( " +
                "username varchar(50) primary key, " +
                "password varchar(50) null);";
        Log.d("Data", "onCreate: " + sql);
        db.execSQL(sql);

        sql = "INSERT INTO login (username, password) " +
                "VALUES ('admin', 'jayapura');";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
    }
}